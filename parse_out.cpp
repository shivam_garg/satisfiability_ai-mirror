#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <vector>
#include <iostream>
using namespace std;

int main(int argc, char** argv)
{
	int G,E,K;
	FILE * p;
	p=fopen(argv[1],"r");
	fscanf(p,"%d %d %d",&G,&E,&K);
	fclose(p);
	p=fopen(argv[2],"r");
	char out[7];
	vector<int> a[K];
	fscanf(p,"%7s",out);
	char s[]="SAT";
	cout << s <<" "<<strlen(s)<< endl;
	cout << out <<" "<< strlen(out)<< endl;
	
	if(!strcmp(out,s)){
		for(int i=0;i<G*K;i++)
		{
			int tmp;
			fscanf(p,"%d",&tmp);
			if(tmp>0)
				a[(tmp-1)%K].push_back((tmp-1)/K);
		}
		fclose(p);
		p=fopen(argv[3],"w");
		for(int i=0;i<K;i++)
		{
			int u=a[i].size();
			fprintf(p,"#%d %d\n",(i+1),u);
			for(int j=0;j<u-1;j++)
				fprintf(p,"%d ",a[i][j]+1);
			fprintf(p, "%d\n", a[i][u-1]+1);
			
		}
		fclose(p);
	}
	else
	{
		fclose(p);
		p=fopen(argv[3],"w");
		fprintf(p,"0\n");
		fclose(p);
	}
}
