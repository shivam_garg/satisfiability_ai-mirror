#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <utility>
#include <string.h>
#include "iostream"

using namespace std;

int G,E,K;
bool **mat;
vector < pair < int , int> > graph;





int main(int argc, char** argv)
{
	FILE * p;
	p=fopen(argv[1],"r");
	fscanf(p,"%d %d %d",&G,&E,&K);
	mat=(bool**)malloc(sizeof(bool*)*(G-1));
	
	// matrix initilaised
	for(int i=0;i<G-1;i++)
	{
		mat[i]=(bool*)malloc(sizeof(bool)*(G-i-1));
		memset(mat[i],false,sizeof(bool)*(G-i-1));
	}
	//matrix being filled according to edges


	int i1,j1;
	for(int i=0;i<E;i++)
	{
		fscanf(p,"%d %d",&i1,&j1);
		graph.push_back(make_pair(i1-1,j1-1));
		if(i1>j1)
		{
			mat[j1-1][i1-j1-1]=true;
		}
		else
		{
			mat[i1-1][j1-i1-1]=true;
		}
	}
	fclose(p);
	long long clauses= 1+ 2*K*E + ((G*(G-1))/2-E)*K + 4*G*(K*(K-1)/2);
	long long var = G*K+(K-1)*E+(G-1)*(K*(K-1));
	ofstream output_file;
	output_file.open(argv[2]);
	output_file <<"p cnf "<<var<<" "<<clauses<<endl;
	//output_file<<"1 0"<< endl;
		// each subgraph has at aleast one element
	// for(int i=0;i<K;i++)
	// {
	// 	for(int j=0;j<G;j++)
	// 	{
	// 		output_file<<j*K+i+1<<" ";
	// 	}
	// 	output_file<<"0"<< endl;
	// }
	// each variable is taken
	// for(int i=0;i<G;i++)
	// {
	// 	for(int j=0;j<K;j++)
	// 	{
	// 		output_file<<j+i*K+1<<" ";
	// 	}
	// 	output_file<<"0"<< endl;
	// }
	// output_file.flush();
	
	//each edge is covered

	

	for(int i=0;i<E;i++)
	{
		int m=graph[i].first;
		int n=graph[i].second;
		
		vector<int> dummy_var;
		for(int j = 0 ; j< K ; j++)
		{
			if(j == K-1)
			{
				output_file<<m*K+j+1 << " " ;
				for(int l = 0 ; l<dummy_var.size() ; l++)
				{
					output_file<< dummy_var[l] << " ";
				}
				output_file << "0" << endl;
				output_file<<n*K+j+1 << " " ;
				for(int l = 0 ; l<dummy_var.size() ; l++)
				{
					output_file<< dummy_var[l] << " ";
				}	
				output_file << "0" << endl;
			}
			else
			{
				dummy_var.push_back(G*K+1+(i*(K-1)+j));
				output_file<<m*K+j+1 << " " ;
				for(int l = 0 ; l<dummy_var.size() ; l++)
				{
					if(l == dummy_var.size()-1) output_file<< -dummy_var[l] << " ";
					else output_file<< dummy_var[l] << " ";
					
				}
				output_file << "0" << endl;
				output_file<<n*K+j+1 << " " ;
				for(int l = 0 ; l<dummy_var.size() ; l++)
				{
					if(l == dummy_var.size()-1) output_file<< -dummy_var[l] << " ";
					else output_file<< dummy_var[l] << " ";
					
				}	
				output_file << "0" << endl;
			}
			
		}
	}
	output_file.flush();


	// for(int i=0;i<E;i++)
	// {
	// 	int m=graph[i].first;
	// 	int n=graph[i].second;
	// 	for(int j=0;j<(1<<K);j++)
	// 	{
			
	// 		int tmp=j;
	// 		for(int k=0;k<K;k++)
	// 		{
	// 			cout << "hi " << j << " " << tmp << endl; 
	// 			if(tmp%2==1)
	// 				output_file<<m*K+k+1<<" ";
	// 			else
	// 				output_file<<n*K+k+1<<" ";
	// 			tmp/=2;
	// 		}
	// 		output_file << "0"<< endl;

	// 	}
	// }
	// output_file.flush();

	//subgraph is complete
	for(int i=0;i<K;i++)
	{
		for(int j=0;j<G-1;j++)
		{
			for(int k=j+1;k<G;k++)
			{
				if(!mat[j][k-j-1])
				{
					output_file << -(j*K+i+1) << " "<< -(k*K+i+1)<< " 0"<< endl;
				}
			}
		}
	}
	output_file.flush();
int curr = 0;
	//subgraphs are disjoint
	for(int i=0;i<K-1;i++)
	{
		for(int j=i+1;j<K;j++)
		{
			vector<int> dummy_var;
			for(int o = 0 ; o<G ; o++)
			{
		
				if(o == G-1)
				{
					output_file<< o*K+i+1 << " " ;
					for(int l = 0 ; l<dummy_var.size() ; l++)
					{
						output_file<< dummy_var[l] << " ";
					}
					output_file << "0" << endl;
					output_file<< -(o*K+j+1) << " " ;
					for(int l = 0 ; l<dummy_var.size() ; l++)
					{
						output_file<< dummy_var[l] << " ";
					}	
					output_file << "0" << endl;
				}
				else
				{
					dummy_var.push_back(G*K+1+(E*(K-1))+curr);
					curr++;
					output_file<<o*K+i+1 << " " ;
					for(int l = 0 ; l<dummy_var.size() ; l++)
					{
						if(l == dummy_var.size()-1) output_file<< -dummy_var[l] << " ";
						else output_file<< dummy_var[l] << " ";
						
					}
					output_file << "0" << endl;
					output_file<< -(o*K+j+1) << " " ;
					for(int l = 0 ; l<dummy_var.size() ; l++)
					{
						if(l == dummy_var.size()-1) output_file<< -dummy_var[l] << " ";
						else output_file<< dummy_var[l] << " ";
						
					}	
					output_file << "0" << endl;
				}

			}
		}
	} 
output_file.flush();
cout << curr << endl;
	for(int i=0;i<K-1;i++)
	{	
		for(int j=i+1;j<K;j++)
		{
			vector<int> dummy_var;
			for(int o = 0 ; o<G ; o++)
			{
		
				if(o == G-1)
				{
					output_file<< -(o*K+i+1) << " " ;
					for(int l = 0 ; l<dummy_var.size() ; l++)
					{
						output_file<< dummy_var[l] << " ";
					}
					output_file << "0" << endl;
					output_file<< o*K+j+1 << " " ;
					for(int l = 0 ; l<dummy_var.size() ; l++)
					{
						output_file<< dummy_var[l] << " ";
					}	
					output_file << "0" << endl;
				}
				else
				{
					dummy_var.push_back(G*K+1+(E*(K-1)) + curr);
					curr++;
					output_file<< -(o*K+i+1) << " " ;
					for(int l = 0 ; l<dummy_var.size() ; l++)
					{
						if(l == dummy_var.size()-1) output_file<< -dummy_var[l] << " ";
						else output_file<< dummy_var[l] << " ";
						
					}
					output_file << "0" << endl;
					output_file<< o*K+j+1 << " " ;
					for(int l = 0 ; l<dummy_var.size() ; l++)
					{
						if(l == dummy_var.size()-1) output_file<< -dummy_var[l] << " ";
						else output_file<< dummy_var[l] << " ";
						
					}	
					output_file << "0" << endl;
				}

			}
		}
	} 
	output_file.flush();


	// for(int i=0;i<K-1;i++)
	// {
	// 	for(int j=i+1;j<K;j++)
	// 	{
	// 		for(int k=0;k<(1<<G);k++)
	// 		{
	// 			int tmp=k;
	// 			for(int o=0;o<G;o++)
	// 			{
	// 				if(tmp%2==0)
	// 				{
	// 					output_file<<o*K+i+1<<" ";
	// 				}
	// 				else
	// 				{
	// 					output_file<< -(o*K+j+1)<<" ";
	// 				}
	// 				tmp/=2;
	// 			}
	// 			output_file << " 0"<< endl;
	// 		}
	// 	}
	// } 
	
	// for(int i=0;i<K-1;i++)
	// {
	// 	for(int j=i+1;j<K;j++)
	// 	{
	// 		for(int k=0;k<(1<<G);k++)
	// 		{
	// 			int tmp=k;
	// 			for(int o=0;o<G;o++)
	// 			{
	// 				if(tmp%2==0)
	// 				{
	// 					output_file<<o*K+j+1<<" ";
	// 				}
	// 				else
	// 				{
	// 					output_file<< -(o*K+i+1)<<" ";
	// 				}
	// 				tmp/=2;
	// 			}
	// 			output_file << " 0"<< endl;
	// 		}
	// 	}
	// }
	// output_file.flush();
	output_file.close();
}
